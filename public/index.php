<?php

/*
 * @copyright Copyright (c) 2024 WinderNet
 *
 * @license MIT License
 *
 * @link https://gitlab.com/windernet/php/framework/-/blob/main/LICENSE
 */

/**
 * This file is as an example entry point for the framework. All this file contains is necessary for using the framework.
 */

//**************************************************************************************************************************************************\\
//                                                                                                                                                  \\
//                                                                  REQUIRED FILES                                                                  \\
//                                                                                                                                                  \\
//**************************************************************************************************************************************************\\

require_once(__DIR__ . '/../vendor/autoload.php');
