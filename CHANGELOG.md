# 0.1.0

* Added module `Common`.
* Added module `Configurations`.

---

### Module `Common`

* Added class `\WinderNet\Common\Application`.
    * It provides the following functions:
        * `path()`
* Added exception `\WinderNet\Common\Exception\InitializationException`.
* Added function `\WinderNet\Common\Function\replace_recursive()`.

---

### Module `Configurations`

* Added class `\WinderNet\Configurations\JsonBasedConfigurations`.
    * It provides the following functions:
        * `get()`
* Added class `\WinderNet\Configurations\PhpBasedConfigurations`.
    * It provides the following functions:
        * `get()`
* Added interface `\WinderNet\Configurations\Configurations`.
    * It provides the following functions:
        * `get()`

---
