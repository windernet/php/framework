# WinderNet PHP Framework

The `WinderNet PHP Framework` is (like the name says) a framework for PHP. It is performant, customizable and runs with plain PHP; no third party
packages are used.

Using only parts (modules) of the framework is also possible.

### Supported PHP Versions
The framework is based on PHP 8.0. Updates to other PHP versions will be done in future.

### Installation
Please, visit the corresponding [wiki page](https://gitlab.com/windernet/php/framework/-/wikis/Installation) for a proper guide on how to install the framework.

### Usage
Please, visit the corresponding [wiki page](https://gitlab.com/windernet/php/framework/-/wikis/How-to-Use) for a proper guide on how to use the framework.

### Support
For bugs and requests, please use the [GitLab issue tracker](https://gitlab.com/windernet/php/framework/-/issues). For questions, please use the [GitLab wiki](https://gitlab.com/windernet/php/framework/-/wikis/home) or see in file documentation.

### License
This project is licensed under the **MIT Licence**. [Learn more](https://opensource.org/license/mit/)
